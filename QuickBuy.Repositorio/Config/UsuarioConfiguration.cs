﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using QuickBuy.Dominio.Entidades;

namespace QuickBuy.Repositorio.Config
{
  public class UsuarioConfiguration : IEntityTypeConfiguration<Usuario>
  {
    public void Configure(EntityTypeBuilder<Usuario> builder)
    {
      builder.HasKey(x => x.Id);

      // Builder usa o padrão Fluent
      builder.Property(x => x.Email)
        .IsRequired()
        .HasMaxLength(50);

      builder.Property(x => x.Senha)
        .IsRequired()
        .HasMaxLength(500);

      builder.Property(x => x.Nome)
        .IsRequired()
        .HasMaxLength(100);

      builder.Property(x => x.SobreNome)
        .IsRequired()
        .HasMaxLength(100);

      builder
        .HasMany(x => x.Pedidos)
        .WithOne(x => x.Usuario);
    }
  }
}
