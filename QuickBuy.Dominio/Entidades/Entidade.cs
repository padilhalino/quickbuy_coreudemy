﻿using System.Collections.Generic;
using System.Linq;

namespace QuickBuy.Dominio.Entidades
{
  public abstract class Entidade
  {
    private List<string> _mensagensValidacao { get; set; }
    private List<string> mensagensValidacao
    {
      get { return _mensagensValidacao ?? (_mensagensValidacao = new List<string>()); }
    }

    protected List<string> ObterMensagensValidacao()
    {
      return mensagensValidacao;
    }

    protected void AdicionarMensagensValidacao(string mensagem)
    {
      mensagensValidacao.Add(mensagem);
    }

    protected void LimparMensagensValidacao()
    {
      mensagensValidacao.Clear();
    }

    public string ObterMensagemValidacao()
    {
      return string.Join(". ", mensagensValidacao);
    }


    public abstract void Validate();
    public bool EhValido
    {
      get { return !mensagensValidacao.Any(); }
    }
  }
}
