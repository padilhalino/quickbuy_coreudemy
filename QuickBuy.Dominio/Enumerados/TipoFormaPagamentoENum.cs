﻿namespace QuickBuy.Dominio.Enumerados
{
  public enum TipoFormaPagamentoEnum
  {
    NaoDefinito = 0,
    Boleto = 1,
    Cartao = 2,
    Deposito = 3
  }
}
