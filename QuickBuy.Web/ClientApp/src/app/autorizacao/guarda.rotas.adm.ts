import { Injectable } from "@angular/core"
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router"
import { Observable } from "rxjs";
import { UsuarioService } from "../services/usuario/usuario.service";

@Injectable({
  providedIn: "root"
})
export class GuardaRotasAdm implements CanActivate {

  constructor(private router: Router, private usuarioService: UsuarioService) {

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {

    if (this.usuarioService.usuario_autenticado()) {
      console.log("já autenticado");
      if (this.usuarioService.usuario_administrador()) {
        return true;
      } else {
        // se o usuário não é adm, volta para a home
        this.router.navigate(["/"], { queryParams: { returnUrl: state.url } });
      }
    }
    else {
      console.log("não autenticado");
      // se o usuário não está autenticado
      this.router.navigate(["/entrar"], { queryParams: { returnUrl: state.url } });
      return false;
    }
  }

}
