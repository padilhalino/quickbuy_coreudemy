"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var GuardaRotasAdm = /** @class */ (function () {
    function GuardaRotasAdm(router, usuarioService) {
        this.router = router;
        this.usuarioService = usuarioService;
    }
    GuardaRotasAdm.prototype.canActivate = function (route, state) {
        if (this.usuarioService.usuario_autenticado()) {
            console.log("já autenticado");
            if (this.usuarioService.usuario_administrador()) {
                return true;
            }
            else {
                // se o usuário não é adm, volta para a home
                this.router.navigate(["/"], { queryParams: { returnUrl: state.url } });
            }
        }
        else {
            console.log("não autenticado");
            // se o usuário não está autenticado
            this.router.navigate(["/entrar"], { queryParams: { returnUrl: state.url } });
            return false;
        }
    };
    GuardaRotasAdm = __decorate([
        core_1.Injectable({
            providedIn: "root"
        })
    ], GuardaRotasAdm);
    return GuardaRotasAdm;
}());
exports.GuardaRotasAdm = GuardaRotasAdm;
//# sourceMappingURL=guarda.rotas.adm.js.map