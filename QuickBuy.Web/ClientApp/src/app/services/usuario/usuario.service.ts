import { Injectable, Inject } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { Usuario } from "../../modelo/Usuario";

@Injectable({
  providedIn: "root"
})
export class UsuarioService {

  private baseURL: string;
  private _usuario: Usuario;

  get headers(): HttpHeaders {
    return new HttpHeaders().set("content-type", "application/json");
  }

  set usuario(usuario: Usuario) {
    sessionStorage.setItem("usuario-autenticado", JSON.stringify(usuario));
    //localStorage.setItem("usuario-autenticado", "1");//Local nunca apaga
    this._usuario = usuario;
  }

  get usuario(): Usuario {
    let usuario_json = sessionStorage.getItem("usuario-autenticado");
    this._usuario = JSON.parse(usuario_json);
    return this._usuario;
  }

  public usuario_autenticado(): boolean {
    return this._usuario != null && this._usuario.email != "" && this._usuario.senha != "";
  }

  public usuario_administrador(): boolean {
    return this._usuario != null && this._usuario.ehAdministrador;
  }

  public limpar_sessao() {
    sessionStorage.setItem("usuario-autenticado", "");
    this._usuario = null;
  }

  constructor(private http: HttpClient, @Inject("BASE_URL") baseUrl: string) {
    this.baseURL = baseUrl;
  }

  public verificarUsuario(usuario: Usuario): Observable<Usuario> {
    //const headers = new HttpHeaders().set("content-type", "application/json");
    //var body = {
    //  email: usuario.email,
    //  senha: usuario.senha
    //};

    //// Ex.: "http://localhost:8080/api/usuario"
    //console.log(this.baseURL + "api/usuario");
    //return this.http.post<Usuario>(this.baseURL + "api/usuario/VerificarUsuario", body, { headers });
    return this.http.post<Usuario>(this.baseURL + "api/usuario/VerificarUsuario", JSON.stringify(usuario), { headers: this.headers });
  }

  public cadastrarUsuario(usuario: Usuario): Observable<Usuario> {
    return this.http.post<Usuario>(this.baseURL + "api/usuario", JSON.stringify(usuario), { headers: this.headers });
  }
}
