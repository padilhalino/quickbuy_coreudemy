"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var UsuarioService = /** @class */ (function () {
    function UsuarioService(http, baseUrl) {
        this.http = http;
        this.baseURL = baseUrl;
    }
    Object.defineProperty(UsuarioService.prototype, "headers", {
        get: function () {
            return new http_1.HttpHeaders().set("content-type", "application/json");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UsuarioService.prototype, "usuario", {
        get: function () {
            var usuario_json = sessionStorage.getItem("usuario-autenticado");
            this._usuario = JSON.parse(usuario_json);
            return this._usuario;
        },
        set: function (usuario) {
            sessionStorage.setItem("usuario-autenticado", JSON.stringify(usuario));
            //localStorage.setItem("usuario-autenticado", "1");//Local nunca apaga
            this._usuario = usuario;
        },
        enumerable: true,
        configurable: true
    });
    UsuarioService.prototype.usuario_autenticado = function () {
        return this._usuario != null && this._usuario.email != "" && this._usuario.senha != "";
    };
    UsuarioService.prototype.usuario_administrador = function () {
        return this._usuario != null && this._usuario.ehAdministrador;
    };
    UsuarioService.prototype.limpar_sessao = function () {
        sessionStorage.setItem("usuario-autenticado", "");
        this._usuario = null;
    };
    UsuarioService.prototype.verificarUsuario = function (usuario) {
        //const headers = new HttpHeaders().set("content-type", "application/json");
        //var body = {
        //  email: usuario.email,
        //  senha: usuario.senha
        //};
        //// Ex.: "http://localhost:8080/api/usuario"
        //console.log(this.baseURL + "api/usuario");
        //return this.http.post<Usuario>(this.baseURL + "api/usuario/VerificarUsuario", body, { headers });
        return this.http.post(this.baseURL + "api/usuario/VerificarUsuario", JSON.stringify(usuario), { headers: this.headers });
    };
    UsuarioService.prototype.cadastrarUsuario = function (usuario) {
        return this.http.post(this.baseURL + "api/usuario", JSON.stringify(usuario), { headers: this.headers });
    };
    UsuarioService = __decorate([
        core_1.Injectable({
            providedIn: "root"
        }),
        __param(1, core_1.Inject("BASE_URL"))
    ], UsuarioService);
    return UsuarioService;
}());
exports.UsuarioService = UsuarioService;
//# sourceMappingURL=usuario.service.js.map