import { Injectable, Inject, OnInit } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { Pedido } from "../../modelo/pedido";

@Injectable({
  providedIn: "root"
})
export class PedidoService implements OnInit {
  private _baseUrl: string;
  public pedidos: Pedido[];

  get headers(): HttpHeaders {
    return new HttpHeaders().set("content-type", "application/json");
  }

  constructor(private http: HttpClient, @Inject("BASE_URL") baseUrl: string) {
    this._baseUrl = baseUrl;
  }

  ngOnInit(): void {
    this.pedidos = [];
  }

  public cadastrar(pedido: Pedido): Observable<Pedido> {
    return this.http.post<Pedido>(this._baseUrl + "api/pedido", JSON.stringify(pedido), { headers: this.headers });
  }
}
