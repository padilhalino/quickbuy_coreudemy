import { Produto } from "../../modelo/produto";

export class LojaCarrinhoCompras {
  public produtos: Produto[] = [];

  public adicionar(produto: Produto) {
    let existe: boolean = false;
    var produtosLocalStorage = localStorage.getItem('produtosLocalStorage');
    if (produtosLocalStorage) {
      this.produtos = JSON.parse(produtosLocalStorage);
      var p = this.produtos.filter(x => x.id == produto.id)[0];
      if (p) {
        existe = true;
        p.quantidade += 1;
      }
    }
    if (!existe) {
      produto.quantidade = 1;
      this.produtos.push(produto);
    }
    localStorage.setItem('produtosLocalStorage', JSON.stringify(this.produtos));
  }

  public obterProdutos(): Produto[] {
    var produtosLocalStorage = localStorage.getItem('produtosLocalStorage');
    if (produtosLocalStorage) {
      this.produtos = JSON.parse(produtosLocalStorage);
    }
    this.produtos.forEach(function (p) {
      p.precoCalculado = p.preco * p.quantidade;
      console.log(p.precoCalculado, p.preco, p.quantidade);
    });
    return this.produtos;
  }

  public temProdutos(): boolean {
    var produtosLocalStorage = localStorage.getItem('produtosLocalStorage');
    if (produtosLocalStorage) {
      this.produtos = JSON.parse(produtosLocalStorage);
      if (this.produtos.length > 0) {
        return true;
      }
    }
    return false;
  }

  public removerProduto(produto: Produto) {
    var produtosLocalStorage = localStorage.getItem('produtosLocalStorage');
    if (produtosLocalStorage) {
      this.produtos = JSON.parse(produtosLocalStorage);
      this.produtos = this.produtos.filter(p => p.id != produto.id);
      if (this.produtos.length > 0) {
        localStorage.setItem('produtosLocalStorage', JSON.stringify(this.produtos));
      } else {
        localStorage.removeItem('produtosLocalStorage');
      }
    }
  }

  public atualizarProdutos(produtos: Produto[]) {
    localStorage.setItem('produtosLocalStorage', JSON.stringify(produtos));
  }

  public limparCarrinho() {
    localStorage.removeItem('produtosLocalStorage');
  }
}
