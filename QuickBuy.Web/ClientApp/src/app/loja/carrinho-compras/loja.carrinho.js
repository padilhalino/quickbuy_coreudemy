"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var LojaCarrinhoCompras = /** @class */ (function () {
    function LojaCarrinhoCompras() {
        this.produtos = [];
    }
    LojaCarrinhoCompras.prototype.adicionar = function (produto) {
        var existe = false;
        var produtosLocalStorage = localStorage.getItem('produtosLocalStorage');
        if (produtosLocalStorage) {
            this.produtos = JSON.parse(produtosLocalStorage);
            var p = this.produtos.filter(function (x) { return x.id == produto.id; })[0];
            if (p) {
                existe = true;
                p.quantidade += 1;
            }
        }
        if (!existe) {
            produto.quantidade = 1;
            this.produtos.push(produto);
        }
        localStorage.setItem('produtosLocalStorage', JSON.stringify(this.produtos));
    };
    LojaCarrinhoCompras.prototype.obterProdutos = function () {
        var produtosLocalStorage = localStorage.getItem('produtosLocalStorage');
        if (produtosLocalStorage) {
            this.produtos = JSON.parse(produtosLocalStorage);
        }
        this.produtos.forEach(function (p) {
            p.precoCalculado = p.preco * p.quantidade;
            console.log(p.precoCalculado, p.preco, p.quantidade);
        });
        return this.produtos;
    };
    LojaCarrinhoCompras.prototype.temProdutos = function () {
        var produtosLocalStorage = localStorage.getItem('produtosLocalStorage');
        if (produtosLocalStorage) {
            this.produtos = JSON.parse(produtosLocalStorage);
            if (this.produtos.length > 0) {
                return true;
            }
        }
        return false;
    };
    LojaCarrinhoCompras.prototype.removerProduto = function (produto) {
        var produtosLocalStorage = localStorage.getItem('produtosLocalStorage');
        if (produtosLocalStorage) {
            this.produtos = JSON.parse(produtosLocalStorage);
            this.produtos = this.produtos.filter(function (p) { return p.id != produto.id; });
            if (this.produtos.length > 0) {
                localStorage.setItem('produtosLocalStorage', JSON.stringify(this.produtos));
            }
            else {
                localStorage.removeItem('produtosLocalStorage');
            }
        }
    };
    LojaCarrinhoCompras.prototype.atualizarProdutos = function (produtos) {
        localStorage.setItem('produtosLocalStorage', JSON.stringify(produtos));
    };
    LojaCarrinhoCompras.prototype.limparCarrinho = function () {
        localStorage.removeItem('produtosLocalStorage');
    };
    return LojaCarrinhoCompras;
}());
exports.LojaCarrinhoCompras = LojaCarrinhoCompras;
//# sourceMappingURL=loja.carrinho.js.map