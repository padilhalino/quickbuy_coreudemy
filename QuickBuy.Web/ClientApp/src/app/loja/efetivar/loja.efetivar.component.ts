import { Component, OnInit } from "@angular/core"
import { Produto } from "../../modelo/produto";
import { Router } from "@angular/router";
import { LojaCarrinhoCompras } from "../carrinho-compras/loja.carrinho";
import { Pedido } from "../../modelo/pedido";
import { UsuarioService } from "../../services/usuario/usuario.service";
import { ItemPedido } from "../../modelo/itemPedido";
import { PedidoService } from "../../services/pedido/pedido.service";

@Component({
  selector: "loja-efetivar",
  templateUrl: "./loja.efetivar.component.html",
  styleUrls: ["./loja.efetivar.component.css"]
})
export class LojaEfetivarComponent implements OnInit {

  public produtos: Produto[];
  public carrinhoCompras: LojaCarrinhoCompras;
  public total: number;
  
  ngOnInit(): void {
    this.carrinhoCompras = new LojaCarrinhoCompras();
    this.produtos = this.carrinhoCompras.obterProdutos();
    this.calcularTotal();
  }

  constructor(private usuarioServico: UsuarioService, private pedidoServico: PedidoService, private router: Router) {
  }

  public atualizarPreco(produto: Produto) {
    produto.precoCalculado = produto.preco * produto.quantidade;
    if (produto.quantidade <= 0) {
      this.remover(produto);
    } else {
      this.carrinhoCompras.atualizarProdutos(this.produtos);
    }
    this.calcularTotal();
  }

  public remover(produto: Produto) {
    this.carrinhoCompras.removerProduto(produto);
    this.produtos = this.carrinhoCompras.obterProdutos();
    this.calcularTotal();
    if (this.produtos.length <= 0) {
      this.router.navigate(['/']);
    }
  }

  public calcularTotal() {
    this.total = this.produtos.reduce((acc, produto) => acc + produto.precoCalculado, 0);
  }

  public efetivarCompra() {
    let pedido = this.criarPedido();
    this.pedidoServico.cadastrar(pedido).subscribe(
      pedidoId => {
        sessionStorage.setItem("pedidoId", pedidoId.toString());
        this.produtos = [];
        this.carrinhoCompras.limparCarrinho();
        this.router.navigate(['/compra-realizada-sucesso']);
      },
      erro => {
        console.log(erro.error);
      }
    );
  }

  public criarPedido(): Pedido {
    let pedido = new Pedido();
    pedido.usuarioId = this.usuarioServico.usuario.id;
    pedido.cep = '81200200';
    pedido.cidade = 'Curitiba';
    pedido.estado = 'PR';
    pedido.enderecoCompleto = "Rua João José Maria";
    pedido.numeroEndereco = "123";
    pedido.dataPrevisaoEntrega = new Date();
    pedido.formaPagamentoId = 1;
    this.produtos = this.carrinhoCompras.obterProdutos();

    for (let p of this.produtos) {
      let itemPedido = new ItemPedido();
      itemPedido.produtoId = p.id;
      itemPedido.quantidade = p.quantidade;
      pedido.itensPedido.push(itemPedido);
    }

    return pedido;
  }
}
