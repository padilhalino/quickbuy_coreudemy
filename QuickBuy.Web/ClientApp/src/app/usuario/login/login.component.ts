import { Component, OnInit } from "@angular/core"
import { Usuario } from "../../modelo/Usuario";
import { Router, ActivatedRoute } from "@angular/router";
import { UsuarioService } from "../../services/usuario/usuario.service";

@Component({
  selector: "login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  public usuario;
  public returnUrl: string;
  public mensagem: string;
  public executando: boolean;

  constructor(private router: Router, private activatedRoute: ActivatedRoute,
    private usuarioService: UsuarioService) {
  }

  ngOnInit(): void {
    this.usuario = new Usuario();
  }


  entrar() {
    this.executando = true;
    this.usuarioService.verificarUsuario(this.usuario).subscribe(
      usuario_json => {
        console.log(usuario_json);
        this.executando = false;
        this.usuarioService.usuario = usuario_json;

        //var usuarioRetorno: Usuario;
        //usuarioRetorno = usuario_json;
        //sessionStorage.setItem("usuario-autenticado", "1");
        //localStorage.setItem("usuario-autenticado", "1");//Local nunca apaga
        //sessionStorage.setItem("email-usuario", usuarioRetorno.email);

        this.returnUrl = this.activatedRoute.snapshot.queryParams["returnUrl"];
        console.log(this.returnUrl);
        if (this.returnUrl === undefined)
          this.router.navigate(["/"]);
        else
          this.router.navigate([this.returnUrl]);
      },
      err => {
        this.executando = false;
        this.mensagem = err.error;
      });

    //if (this.usuario.email == "padilhalino@hotmail.com" && this.usuario.senha == "123") {
    //  sessionStorage.setItem("usuario-autenticado", "1");
    //  //localStorage.setItem("usuario-autenticado", "1");//Local nunca apaga
    //  this.returnUrl = this.activatedRoute.snapshot.queryParams["returnUrl"];
    //  this.router.navigate([this.returnUrl]);
    //}
  }
}
