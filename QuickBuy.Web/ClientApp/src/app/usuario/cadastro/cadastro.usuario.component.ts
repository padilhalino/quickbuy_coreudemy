import { Component, OnInit } from "@angular/core"
import { Usuario } from "../../modelo/Usuario"
import { UsuarioService } from "../../services/usuario/usuario.service";

@Component({
  selector: "cadastro-usuario",
  templateUrl: "./cadastro.usuario.component.html",
  styleUrls: ["./cadastro.usuario.component.css"]
})
export class CadastroUsuarioComponent implements OnInit {
  public usuario: Usuario;
  public executando: boolean;
  public mensagem: string;
  public usuarioCadastrado: boolean;

  constructor(private usuarioServico: UsuarioService) {

  }
  ngOnInit(): void {
    this.usuario = new Usuario();
  }

  public cadastrarUsuario() {
    this.executando = true;
    this.usuarioServico.cadastrarUsuario(this.usuario).subscribe(
      usuarioJson => {
        this.executando = false;
        this.usuarioCadastrado = true;
        this.mensagem = "";
      },
      erro => {
        this.executando = false;
        console.log(erro);
        this.mensagem = erro.error;
      }
    );
  }
}
