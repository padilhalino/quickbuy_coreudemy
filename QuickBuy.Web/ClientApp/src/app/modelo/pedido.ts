import { ItemPedido } from "./itemPedido";

export class Pedido {
  id: number;
  dataPedido: Date;
  usuarioId: number;
  dataPrevisaoEntrega: Date;
  cep: string;
  cidade: string;
  estado: string;
  enderecoCompleto: string;
  numeroEndereco: string;
  formaPagamentoId: number;
  itensPedido: ItemPedido[];

  constructor() {
    this.dataPedido = new Date();
    this.itensPedido = [];
  }
}
