export class Produto {
  id: number;
  nome: string;
  descricao: string;
  preco: number;
  nomeArquivo: string;
  precoCalculado: number;
  quantidade: number;
}
