import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsuarioService } from '../services/usuario/usuario.service';
import { LojaCarrinhoCompras } from '../loja/carrinho-compras/loja.carrinho';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnInit {

  isExpanded = false;
  public carrinhoCompras: LojaCarrinhoCompras;

  ngOnInit(): void {
    this.carrinhoCompras = new LojaCarrinhoCompras();
  }

  constructor(private router: Router, private usuarioService: UsuarioService) {
  }

  collapse() {
    this.isExpanded = false;
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }

  public usuarioLogado(): boolean {
    return this.usuarioService.usuario_autenticado();
  }

  sair() {
    this.usuarioService.limpar_sessao();
    //this.router.navigate(["/entrar"]);
    this.router.navigate(["/"]);
  }

  get usuario() {
    return this.usuarioService.usuario;
  }

  public usuarioAdm(): boolean {
    return this.usuarioService.usuario_administrador();
  }

  public temItensCarrinho(): boolean {
    return this.carrinhoCompras.temProdutos();
  }
}
