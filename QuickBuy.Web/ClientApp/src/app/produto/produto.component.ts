import { Component, OnInit } from "@angular/core"
import { Produto } from "../modelo/produto";
import { ProdutoServico } from "../services/produto/produto.service";
import { Router } from "@angular/router";

@Component({
  selector: "produto",
  templateUrl: "./produto.component.html",
  styleUrls: ["./produto.component.css"]
})
export class ProdutoComponent implements OnInit {
  public produto: Produto;
  public arquivoSelecionado: File;
  public ativar_spinner: boolean;
  public mensagem: string;

  constructor(private produtoServico: ProdutoServico, private router: Router) {

  }

  ngOnInit(): void {
    var produtoSession = sessionStorage.getItem('produtoSession');
    if (produtoSession) {
      this.produto = JSON.parse(produtoSession);
      sessionStorage.removeItem('produtoSession');
    } else {
      this.produto = new Produto();
    }
  }


  nome: string;
  liberadoParaVenda: boolean;

  //public obterNome() {
  //  return "blá blá blá";
  //}

  public inputFileChange(files: FileList) {
    this.arquivoSelecionado = files.item(0);
    this.ativar_spinner = true;
    this.produtoServico.enviarArquivo(this.arquivoSelecionado).subscribe(
      nomeArquivo => {
        this.produto.nomeArquivo = nomeArquivo;
        console.log(nomeArquivo);
        this.ativar_spinner = false;
      },
      erro => {
        console.log("Erro!", erro);
        this.ativar_spinner = false;
      }
    );
  }

  public cadastrar() {
    this.ativarEspera();
    this.produtoServico.cadastrar(this.produto).subscribe(
      produtoJson => {
        console.log(produtoJson);
        this.mensagem = "";
        this.desativarEspera();
        this.router.navigate(['/pesquisar-produto']);
      },
      erro => {
        console.log(erro.error);
        this.mensagem = erro.error;
        this.desativarEspera();
      }
    );
  }

  public ativarEspera() {
    this.ativar_spinner = true;
  }

  public desativarEspera() {
    this.ativar_spinner = false;
  }
}
