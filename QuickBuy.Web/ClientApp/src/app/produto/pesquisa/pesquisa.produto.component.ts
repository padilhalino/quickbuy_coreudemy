import { Component, OnInit } from "@angular/core"
import { Produto } from "../../modelo/produto";
import { ProdutoServico } from "../../services/produto/produto.service";
import { Router } from "@angular/router";

@Component({
  selector: "pesquisa-produto",
  templateUrl: "./pesquisa.produto.component.html",
  styleUrls: ["./pesquisa.produto.component.css"]
})
export class PesquisaProdutoComponent implements OnInit {

  public produtos: Produto[];

  ngOnInit(): void {
    
  }

  constructor(private produtoServico: ProdutoServico, private router: Router) {
    this.produtoServico.obterTodosProdutos()
      .subscribe(
        produtos_json => {
          this.produtos = produtos_json;
          console.log(produtos_json);
        },
        e => {
          console.log(e.error);
        }
      );
  }

  public adicionarProduto() {
    this.router.navigate(['/produto']);
  }

  public deletarProduto(produto: Produto) {
    var confirmado = confirm(`Deseja realmente deletar o produto "${produto.nome}"`);
    if (confirmado) {
      this.produtoServico.deletar(produto).subscribe(
        produtos_json => {
          this.produtos = produtos_json;
          console.log(produtos_json);
        },
        e => { console.log(e.error) }
      );
    }
  }

  public editarProduto(produto: Produto) {
    sessionStorage.setItem('produtoSession', JSON.stringify(produto));
    this.router.navigate(['/produto']);
  }
}
