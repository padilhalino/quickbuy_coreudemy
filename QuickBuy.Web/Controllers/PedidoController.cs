﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using QuickBuy.Dominio.Contratos;
using QuickBuy.Dominio.Entidades;
using System;

namespace QuickBuy.Web.Controllers
{
  [Route("api/[controller]")]
  public class PedidoController : Controller
  {
    private readonly IPedidoRepositorio _pedidoRepositorio;

    public PedidoController(IPedidoRepositorio pedidoRepositorio)
    {
      _pedidoRepositorio = pedidoRepositorio;
    }

    [HttpGet]
    public IActionResult Get()
    {
      try
      {
        return Json(_pedidoRepositorio.ObterTodos());
      }
      catch (Exception ex)
      {
        return BadRequest(ex.ToString());
      }
    }

    [HttpPost]
    public IActionResult Post([FromBody]Pedido pedido)
    {
      try
      {
        _pedidoRepositorio.Adicionar(pedido);
        return Ok(pedido.Id);
      }
      catch (Exception ex)
      {
        return BadRequest(ex.ToString());
      }
    }
  }
}
